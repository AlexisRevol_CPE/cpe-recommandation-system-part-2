
from Services.UserItemMatrix import UserItemMatrix
from Services.ImageContentBased import ImageContentBased
from Services.Classification import Classification
import os
import json

def test_classification(metadata):
    """
    Tester les différents classificateurs : MLP, SVC, DTC, KNN, RFC, Perceptron
    """
    classificateur = Classification(metadata)
    classificateur.compute_MLP()
    classificateur.compute_SVC()
    classificateur.compute_DTC()
    classificateur.compute_KNN()
    classificateur.compute_RandomForest()
    classificateur.compute_Perceptron()


if __name__ == '__main__':
    dossier_json = os.environ.get("DOSSIER_JSON", "/app/data/json")
    metadata_json = f"{dossier_json}/metadata.json"
    user_preferences_json = f"{dossier_json}/user_preference.json"

    # Load user preferences
    with open(user_preferences_json, 'r') as f:
        user_preferences = json.load(f)

    # Load image metadata
    with open(metadata_json, 'r') as f:
        metadata = json.load(f)

    # Tester les classificateurs
    test_classification(metadata)

    user_id = 1
    filtrage_collaboratif = False


    if filtrage_collaboratif:
        print("Recommendation utilisant le filtrage collaboratif")
        # Filtrage collaboratif
        user_item = UserItemMatrix(user_preferences, metadata)

       
        recommended_items = user_item.get_recommendations(user_id, n=5)
        print("Recommended items for user", user_id, ":", recommended_items)

    else:
        print("Recommendation basé sur le contenu")
        # Basé sur le contenu
        recommender = ImageContentBased(metadata_json, user_preferences_json)


        favorite_image = recommender.get_favorite_images(user_id)
        recomended_images = recommender.get_preferences(favorite_image)

        print(f"Recommandation for user{user_id}: {recomended_images}")
