import json
import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder
from sklearn.cluster import KMeans
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer


class ImageContentBased:
    def __init__(self, metadata_file_path, user_preferences_file_path):
        self.metadata_file_path = metadata_file_path
        self.user_preferences_file_path = user_preferences_file_path
        self.metadata = self.load_metadata()
        self.user_preferences = None

        self.load_user_preferences()

    def load_metadata(self):
        """
        Load image metadata from file
        """
        with open(self.metadata_file_path, 'r') as f:
            metadata = json.load(f)
        return metadata

    def load_user_preferences(self):
        """
        Load user preferences from file
        """
        with open(self.user_preferences_file_path, 'r') as f:
            self.user_preferences = json.load(f)

    def get_favorite_images(self, user_id):
        """
        Return favorite images of the user
        """
        for user in self.user_preferences:
            if user['user_id'] == user_id:
                return user['favorite_images']
        return []

    def get_preferences(self, liked_images):
        """
        Return recommended images for a user based on their liked images
        """
        # Prepare training and test data
        X = []  # image features
        y = []  # 0 if user does not like the image, 1 if user likes the image

        # Create a text vectorizer
        vectorizer = CountVectorizer()

        # Add the features of image and user's response for image i to y
        X = list(map(lambda key: [self.metadata[key]['cluster_rgb'], self.metadata[key]['cluster_variance']], self.metadata))
        y = list(map(lambda key: 1 if key in liked_images else 0, self.metadata))

        # Split the data into training and test sets
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        # Train the decision tree
        dt = DecisionTreeClassifier()
        dt.fit(X_train, y_train)

        recommended_images = []
 
        # Use functional programming to generate recommended images
        not_liked_images = list(filter(lambda key: key not in liked_images, self.metadata))
        recommended_images = list(filter(lambda key: dt.predict([[self.metadata[key]['cluster_rgb'], self.metadata[key]['cluster_variance']]])[0] == 1, not_liked_images))

        return recommended_images



