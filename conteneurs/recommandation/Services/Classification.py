from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import Perceptron
from sklearn.metrics import classification_report, confusion_matrix, precision_recall_curve, roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from collections import Counter
import random
import matplotlib.pyplot as plt
import Services as tool
import os
from functools import reduce

class Classification:
    def __init__(self, metadata):
        self.metadata = metadata
        self.vectorizer = CountVectorizer()

        self.output_folder = f'{os.environ.get("DOSSIER_CHARTS", "/app/data/charts")}/classification'

        # Sélectionner aléatoirement n images aimées par l'utilisateur
        self.n = 40
        self.liked_images = set()
        while len(self.liked_images) < self.n:
            self.liked_images.add(random.randint(0, 100))
        self.liked_images = list(self.liked_images)

        # Préparer les données d'entraînement et de test
        self.X = []  # caractéristiques des images
        self.y = []  # 0 si l'utilisateur n'aime pas l'image, 1 si l'utilisateur aime l'image
        

        # Ajouter les caractéristiques de l'image i à X
        print(self.metadata.items())
        get_image_features = lambda key, value: [value['cluster_rgb'], value['cluster_variance']]
        #get_image_features = lambda key, value: print(value)
        
        self.X = list(map(lambda x: get_image_features(*x), self.metadata.items()))

        # Ajouter la réponse de l'utilisateur pour l'image i à y
        get_user_response = lambda i: 1 if i in self.liked_images else 0
        self.y = list(map(get_user_response, range(len(self.metadata))))


        # Fractionnement des données en ensembles d'entraînement et de test
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y, test_size=0.2,
                                                                                  random_state=42)
        
    # Convertir la caractéristique 'tags' en un vecteur numérique à l'aide du vectoriseur de texte
    def get_tags_vector(self, tags):
        try:
            return self.vectorizer.fit_transform([tags]).toarray()[0][0]
        except:
            return 0


    def add_features(self, i):
        # Ajouter les caractéristiques de l'image i à X
        self.X.append([self.metadata[i]['cluster_rgb'], self.metadata[i]['cluster_variance']])

        # Ajouter la réponse de l'utilisateur pour l'image i à y
        if i in self.liked_images:
            self.y.append(1)
        else:
            self.y.append(0)
    
        return self.X, self.y

    def train_MLP(self):
        # Entraînement du réseau de neurones
        mlp = MLPClassifier(hidden_layer_sizes=(100,), max_iter=1000)
        # Entraîner le modèle sur les données d'entraînement
        mlp.fit(self.X_train, self.y_train)

        # Faire des prédictions sur l'ensemble de test
        y_pred = mlp.predict(self.X_test)
        # Obtenir les probabilités pour chaque classe (0 ou 1) pour chaque exemple dans l'ensemble de test
        y_prob = mlp.predict_proba(self.X_test)
        # Extraire la probabilité pour la classe positive (1)
        y_prob = y_prob[:, 1]
        # Draw repartion charts
        self.repartition_chart(mlp, "MLP")

        return y_pred, y_prob
       

    def train_SVC(self):
        # Entraînement du modèle SVC
        svc = SVC(probability=True)
        svc.fit(self.X_train, self.y_train)

        # Faire des prédictions sur l'ensemble de test
        y_pred = svc.predict(self.X_test)
        # Obtenir les probabilités pour chaque classe (0 ou 1) pour chaque exemple dans l'ensemble de test
        y_prob = svc.predict_proba(self.X_test)
        # Extraire la probabilité pour la classe positive (1)
        y_prob = y_prob[:, 1]

        return y_pred, y_prob

    def train_DTC(self):
        # Entraînement du modèle DecisionTreeClassifier
        dtc = DecisionTreeClassifier(random_state=42)
        dtc.fit(self.X_train, self.y_train)

        # Faire des prédictions sur l'ensemble de test
        y_pred = dtc.predict(self.X_test)
        # Obtenir les probabilités pour chaque classe (0 ou 1) pour chaque exemple dans l'ensemble de test
        y_prob = dtc.predict_proba(self.X_test)
        # Extraire la probabilité pour la classe positive (1)
        y_prob = y_prob[:, 1]

        return y_pred, y_prob

    def train_KNN(self):
        # Entraînement du modèle DecisionTreeClassifier
        knn = KNeighborsClassifier(n_neighbors=5)
        knn.fit(self.X_train, self.y_train)

        # Faire des prédictions sur l'ensemble de test
        y_pred = knn.predict(self.X_test)
        # Obtenir les probabilités pour chaque classe (0 ou 1) pour chaque exemple dans l'ensemble de test
        y_prob = knn.predict_proba(self.X_test)
        # Extraire la probabilité pour la classe positive (1)
        y_prob = y_prob[:, 1]

        return y_pred, y_prob
    
    def train_RandomForest(self):
        # Entraînement du modèle DecisionTreeClassifier
        rdc = RandomForestClassifier()
        rdc.fit(self.X_train, self.y_train)

        # Faire des prédictions sur l'ensemble de test
        y_pred = rdc.predict(self.X_test)
        # Obtenir les probabilités pour chaque classe (0 ou 1) pour chaque exemple dans l'ensemble de test
        y_prob = rdc.predict_proba(self.X_test)
        # Extraire la probabilité pour la classe positive (1)
        y_prob = y_prob[:, 1]

        return y_pred, y_prob

    def train_Perceptron(self):
        # Entraînement du modèle DecisionTreeClassifier
        perceptron = Perceptron()
        perceptron.fit(self.X_train, self.y_train)

        # Faire des prédictions sur l'ensemble de test
        y_pred = perceptron.predict(self.X_test)
        # Obtenir les probabilités pour chaque classe (0 ou 1) pour chaque exemple dans l'ensemble de test
        y_prob = perceptron.decision_function(self.X_test)
        return y_pred, y_prob


    def compute_model(self, y_pred, y_prob, name):
        # Calculer les métriques de classification pour le modèle MLPClassifier
        print(f"{name}Classifier:")
        print(classification_report(self.y_test, y_pred))
        print("Confusion Matrix:")
        print(confusion_matrix(self.y_test, y_pred))
        precision, recall, thresholds = precision_recall_curve(self.y_test, y_prob)
        plt.clf()
        plt.plot(recall, precision)
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.title(f'Precision-Recall Curve for {name}Classifier')
        plt.savefig(f"{self.output_folder}/{name}_recallcurve.png")

        fpr, tpr, thresholds = roc_curve(self.y_test, y_prob)
        roc_auc = auc(fpr, tpr)
        plt.clf()
        plt.plot(fpr, tpr)
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC Curve for MLPClassifier (AUC = %0.2f)' % roc_auc)
        print(f"saving chart under {self.output_folder}/{name}.png")
        plt.savefig(f"{self.output_folder}/{name}_rate.png")


        
    def compute_MLP(self):
        # Entraîner le modèle MLPClassifier
        y_mlp_pred, y_mlp_prob = self.train_MLP()
        self.compute_model(y_mlp_pred, y_mlp_prob, "MLP")

    def compute_SVC(self):
        # Entraîner le modèle SVC
        y_svc_pred, y_svc_prob = self.train_SVC()
        self.compute_model(y_svc_pred, y_svc_prob, "SVC")


    def compute_DTC(self):
        # Entraîner le modèle DTC
        y_dtc_pred, y_dtc_prob = self.train_DTC()
        self.compute_model(y_dtc_pred, y_dtc_prob, "DTC")


    def compute_KNN(self):
        # Entraîner le modèle KNN
        y_knn_pred, y_knn_prob = self.train_KNN()
        self.compute_model(y_knn_pred, y_knn_prob, "KNN")


    def compute_RandomForest(self):
        # Entraîner le modèle RandomForest
        y_forest_pred, y_forest_prob = self.train_RandomForest()
        self.compute_model(y_forest_pred, y_forest_prob, "RandomForest")


    def compute_Perceptron(self):
        # Entraîner le modèle Perceptron
        y_perceptron_pred, y_perceptron_prob = self.train_Perceptron()
        self.compute_model(y_perceptron_pred, y_perceptron_prob, "Perceptron")

        
    def repartition_chart(self, model, name):
        try:
            # Récupérer les positions des images par rapport au centre de leur cluster
            image_positions = []
            # Recommander les images similaires à celles aimées par l'utilisateur
            recommended_images = []

            # Obtenir les positions des images et les images recommandées en utilisant la méthode map
            image_positions = list(map(lambda key: (self.metadata[key]['cluster_rgb'], self.metadata[key]['cluster_variance'], 'liked') if key in self.liked_images else (self.metadata[key]['cluster_rgb'], self.metadata[key]['cluster_variance'], 'recommended') if model.predict([[self.metadata[key]['cluster_rgb'], self.metadata[key]['cluster_variance']]])[0] == 1 else (self.metadata[key]['cluster_rgb'], self.metadata[key]['cluster_variance'], 'not recommended'), self.metadata.keys()))


            """
            Draw charts to visualize results
            """
            ###
            # Créer un dictionnaire pour associer les étiquettes de couleur à chaque catégorie d'image
            colors = {'liked': 'yellow', 'recommended': 'blue', 'not recommended': 'red'}

            # Compter le nombre de points à chaque emplacement
            count = Counter(image_positions)

            # 7. Créer un nuage de points en 3D avec des couleurs pour représenter le tag
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

            # Appliquer la fonction map pour mettre à jour la taille des points
            sizes = list(map(lambda size: 100*size if size > 1 else 20, count.values()))

            # Appliquer la fonction map pour transformer les tuples en arguments pour la fonction scatter
            points = list(map(lambda x: (x[0][0], x[0][1], x[0][2], colors[x[0][3]], x[1]), count.items()))

            # Appliquer la fonction zip pour créer des listes séparées pour chaque argument
            x, y, z, c, s = zip(*points)

            # Afficher le nuage de points
            ax.scatter(x, y, z, c=c, alpha=0.5, s=s)

            # Ajouter les étiquettes des axes et le titre du graphique
            ax.set_xlabel('Cluster RGB')
            ax.set_ylabel('Cluster Variance')
            ax.set_zlabel('Tag')
            plt.title('Répartition des images dans les 3 catégories')

            # Afficher le graphique et enregistrer l'image
            plt.savefig(f"{self.output_folder}/{name}_repartition.png")
            plt.show()
        except:
            pass
