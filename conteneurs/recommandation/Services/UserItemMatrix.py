import json
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity


class UserItemMatrix:
    """
    Système de recommendation basé sur le filtrage collaboratif
    """
    def __init__(self, user_preferences, metadata):
        self.user_preferences = user_preferences
        self.metadata = metadata
        self.n_users = len(user_preferences)
        self.n_items = len(metadata)
        self.user_item_matrix = self.create_user_item_matrix()
        self.user_similarity_matrix = self.compute_user_similarity_matrix()
        
    def create_user_item_matrix(self):
        """
        Crée une matrice utilisateur-image en utilisant les préférences de l'utilisateur et les métadonnées de 
        l'image. Chaque utilisateur estreprésenté par une ligne dans la matrice et chaque image est représentée 
        par une colonne.
        Si un utilisateur a marqué une image comme favorite, la valeur correspondante dans la matrice est 1, 
        sinon la valeur est 0.
        """
        metadata_keys = list(self.metadata.keys())
        user_item_matrix = np.array(list(map(lambda user: 
            list(map(lambda item: 1 if item in user['favorite_images'] else 0, metadata_keys)), 
            self.user_preferences)))
    
        return user_item_matrix
    
    def compute_user_similarity_matrix(self):
        """
        Calcule la similarité entre les utilisateurs en utilisant la similarité cosinus. 
        La matrice de similarité cosinus est une matrice carrée dont chaque élément (i,j) représente la 
        similarité cosinus entre les utilisateurs i et j.
        """
        user_similarity_matrix = cosine_similarity(self.user_item_matrix)
        return user_similarity_matrix
    
    def get_similar_users(self, user_id, n=5):
        """"
        Renvoie les n utilisateurs les plus similaires. Les utilisateurs sont classés par ordre décroissant de 
        similarité cosinus.
        """
        similarities = self.user_similarity_matrix[user_id]
        most_similar_users = np.argsort(similarities)[-n-1:-1][::-1]
        return most_similar_users
    
    def predict_ratings(self, user_id, item_id):
        """
        Calcule la note que l'utilisateur donnerait à l'image en se basant sur les notes données par 
        les utilisateurs similaires
        """
        similar_users = self.get_similar_users(user_id)
        item_ratings = self.user_item_matrix[similar_users, item_id]
        item_similarities = self.user_similarity_matrix[user_id, similar_users]
        prediction = np.dot(item_ratings, item_similarities) / item_similarities.sum()
        return prediction
    
    def get_recommendations(self, user_id, n=5):
        """
        Calcule la note prédite pour chaque image non marquée comme favorite par l'utilisateur et renvoie 
        les n images les mieux notées.
        """
        unrated_items = np.where(self.user_item_matrix[user_id] == 0)[0]
        """
        predicted_ratings = []
        for item_id in unrated_items:
            prediction = self.predict_ratings(user_id, item_id)
            predicted_ratings.append((self.metadata[list(self.metadata.keys())[item_id]]['filename'], prediction))
        predicted_ratings.sort(key=lambda x: x[1], reverse=True)
        recommended_items = [item[0] for item in predicted_ratings[:n]]
      
        """
        ###
        predicted_ratings = list(map(lambda item_id: 
            (self.metadata[list(self.metadata.keys())[item_id]]['filename'], self.predict_ratings(user_id, item_id)), 
            unrated_items))
        predicted_ratings.sort(key=lambda x: x[1], reverse=True)
        recommended_items = list(map(lambda item: item[0], predicted_ratings[:n]))
        return recommended_items

