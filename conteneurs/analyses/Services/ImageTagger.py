import os
import json
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2, preprocess_input, decode_predictions
from tensorflow.keras.preprocessing.image import load_img, img_to_array
import numpy as np
from tqdm import tqdm
from functools import reduce

class ImageTagger:
    def __init__(self, dossier_image):
        self.dossier_image = dossier_image
        self.model = MobileNetV2(weights='imagenet')

        self.json_file = f'{os.environ.get("DOSSIER_JSON", "/app/data/json")}/metadata.json'
        self.metadata = self.load_metadata(self.json_file)


    def is_file_valid(self, file):
        """
        Return True if the file name is valid
        """
        return (file.endswith('.jpg') or file.endswith('.png')) and not file.startswith('.')


    def tag_image(self, file):
        if self.is_file_valid(file):
            print(f"\rLoading and resizing {file}\t", end='')

            # Chemin de l'image
            image_path = os.path.join(self.dossier_image, file)

            try:
                # Charger et redimensionner l'image
                image = load_img(image_path, target_size=(224, 224))
                image = img_to_array(image)

                # Prétraiter l'image
                image = preprocess_input(image)
                image = np.expand_dims(image, axis=0)

                # Prédire le tag de l'image
                predictions = self.model.predict(image)
                tags = decode_predictions(predictions, top=10)[0]
        
                self.metadata[file]["tags"] = list(map(lambda tag: tag[1], tags[:3]))

                # Renvoyer les tags
                return (file, [tag[1] for tag in tags[:3]])
            except:
                #self.metadata[file]["tags"] = list()
                return (file, ["default"])

        # Renvoyer None si le fichier n'est pas valide
        return "unknown"

    def add_tags(self, tags_dict, tags):
        file, tag_list = tags
        tags_dict[file] = {"tags": tag_list}
        return tags_dict

    def tag_automatisation(self):
        # Obtenir la liste des fichiers d'images
        files = os.listdir(self.dossier_image)

        print("Tagging images...")

        # Dictionnaire pour stocker les tags pour chaque image
        tags_dict =  list(map(lambda file: self.tag_image(file), files))
        
        self.write_metadata(self.metadata)


    def load_metadata(self, metadata_file):
        with open(metadata_file, "r") as f:
            return json.load(f)

    def write_metadata(self, metadata):
        # Enregistrer les tags dans un fichier metadata.json
        with open(self.json_file, 'w') as f:
            json.dump(metadata, f, indent=4)
