import json
import pandas as pd
import numpy as np
import random
import Services as tool
from functools import reduce

class UserPreferenceProfile:
    def __init__(self, user_id, metadata):
        self.user_id = user_id
        self.metadata = metadata
        self.liked_images = self.get_liked_images()
        self.preferences = self.get_preferences()

    def get_liked_images(self, selection_random=True):
        liked_images = set()
        if selection_random:
            # Sélectionner aléatoirement n images aimées par l'utilisateur
            n = 40
            liked_images = set(random.randint(0, 100) for _ in range(n))
        else:
            # Select num_images random images
            num_images = 40
            image_ids = random.sample(list(self.metadata.keys()), num_images)

            ##
            liked_images = set(filter(lambda image_id: self.ask_question(image_id), image_ids))
        return list(liked_images)

    def ask_question(self, image_id):
        print(f"Do you like image {image_id}? (y/n)")
        response = input().lower()
        liked = False
        while response not in ['y', 'n']:
            print("Invalid response. Please enter 'y' or 'n'.")
            response = input().lower()
        if response == 'y':
            liked = True
        return liked

    def get_preferences(self):
        """
        return preferences of a user
        """
        preference_values = {"favorite_images": []}

        try:
            # Sélectionner uniquement les images aimées
            liked_metadata = list(filter(lambda x: x[0] in self.liked_images, enumerate(self.metadata)))

            # Convertir chaque image aimée en sa clé correspondante
            liked_images = list(map(lambda x: x[1], liked_metadata))

            # Ajouter chaque image aimée à la liste de favoris
            preference_values["favorite_images"] = reduce(lambda x, y: x + [y], liked_images, [])

            image_positions = []

            # Appliquer la fonction calculate_positions à chaque image
            image_positions = list(map(lambda x: self.calculate_positions(*x, self.liked_images), enumerate(self.metadata)))

          
            user_data = pd.DataFrame(image_positions, columns=['cluster_rgb', 
                                                                'cluster_variance', 
                                                                'tags', 'size', 
                                                                'mode', 'rgb', 
                                                                'average_rgb', 'prediction'
                                                                ])
        
            recommended_user_data = user_data[(user_data['prediction'] == 'liked')]

            # Trouver la valeur qui a la fréquence d'apparition la plus grande dans la colonne
            preference_values.update(dict(zip(recommended_user_data.columns,
                                        recommended_user_data.apply(lambda x: x.value_counts().idxmax())))
                                )
        except:
            print("Cant establish user preferences...")
        
        return preference_values

    # Créer une fonction pour calculer les positions de chaque image
    def calculate_positions(self, i, key, liked_images):
        try:
            positions = [
                self.metadata[key]['cluster_rgb'], 
                self.metadata[key]['cluster_variance'], 
                self.metadata[key]['tags'], 
                self.metadata[key]['size'], 
                self.metadata[key]['mode'], 
                self.metadata[key]['rgb'], 
                self.metadata[key]['average_rgb']
            ]
        except:
            positions = [-1, -1, ["unknown"], "unknown", "unknown", [-1,-1,-1], [-1,-1,-1]]

        if i in liked_images:
            positions.append('liked')
        else:
            positions.append('not liked')
            return positions
