from sklearn.preprocessing import OneHotEncoder
from Entity.NumpyEncoder import NumpyEncoder
from Services.UserPreferenceProfile import UserPreferenceProfile
import json

class UserManager:
    def __init__(self, dossier_json):
        self.metadata_file = f"{dossier_json}/metadata.json"
        self.user_file = f"{dossier_json}/user_preference.json"

        # Charger les metadata depuis le fichier JSON
        self.metadata = self.load_metadata(self.metadata_file)

        
    def create_users(self, num_users=10):
        users = []
        #print(self.metadata)
        #print()
        users = list(map(lambda i: {'user_id': i + 1, **UserPreferenceProfile(i, self.metadata).get_preferences()},
                 range(num_users)))

        return users

    def write_preferences(self, users):
        # Save user preferences into JSON file
        with open(self.user_file, 'w') as outfile:
            json.dump(users, outfile, indent=4, cls=NumpyEncoder)

    def load_metadata(self, metadata_file):
        with open(metadata_file, "r") as f:
            return json.load(f)
