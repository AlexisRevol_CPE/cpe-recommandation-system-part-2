import json
from sklearn.cluster import KMeans
from datetime import datetime
from dateutil import parser
import Services as tool
import matplotlib.pyplot as plt
from functools import reduce

class ImageClustering:
    def __init__(self, metadata_file):
        self.metadata = self.load_metadata(metadata_file)

    def load_metadata(self, metadata_file):
        with open(metadata_file, "r") as f:
            return json.load(f)

    def determine_optimal_k(self, feature_name):
        #data = [self.metadata[key][feature_name] for key in self.metadata]
        data = list(map(lambda key: self.metadata[key][feature_name], self.metadata))

        # Store sum of squared distances values
        sum_of_squared_distances = []

        # Tester K-means pour différentes valeurs de K
        K = range(1, 10)
        sum_of_squared_distances = list(map(lambda k: KMeans(n_clusters=k, n_init=10).fit(data).inertia_, K))


        # Plot sum of squared distances vs K to determine optimal value
        plt.plot(K, sum_of_squared_distances, 'bx-')
        plt.xlabel('Number of clusters (K)')
        plt.ylabel('Sum of squared distances')
        plt.title('Elbow method to determine optimal K')
        plt.show()

    def cluster_images(self, cluster_name, feature_name, K):
        print(f"Performing clustering on {feature_name}...")
  
        X = list(map(lambda key: self.metadata[key][feature_name], self.metadata))

        # Perform clustering
        kmeans = KMeans(n_clusters=K, random_state=0, n_init=10).fit(X)

        # Convert label array to Python list
        labels = kmeans.labels_.tolist()

        # Add cluster metadata to each image
        #for i, key in enumerate(self.metadata):
        #    self.metadata[key][cluster_name] = labels[i]
        
        # Add cluster metadata to each image
        list(map(lambda x: self.metadata[x[0]].__setitem__(cluster_name, x[1]), zip(self.metadata.keys(), labels)))


    def cluster_period(self):
        features = []

        print("Performing clustering on period...")

        features = list(map(lambda data: self.set_period(data[0], data[1]), self.metadata.items()))
        
        # Add period metadata to each image
        #for i, key in enumerate(self.metadata):
        #     self.metadata[key]['period'] = features[i]

    def set_period(self, key, period_value):
        if 6 <= parser.parse(period_value['DateTime']).time().hour < 12:
            self.metadata[key]['period'] = "morning"
        elif 12 <= parser.parse(period_value['DateTime']).time().hour < 18:
            self.metadata[key]['period'] = "day"
        else:
            self.metadata[key]['period'] = "night"
                  

    def write_metadata(self, metadata_file):
        with open(metadata_file, "w") as f:
            json.dump(self.metadata, f, indent=4)

