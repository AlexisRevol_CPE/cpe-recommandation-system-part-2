from Services.ImageClustering import ImageClustering
from Services.ImageTagger import ImageTagger
from Services.UserManager import UserManager
import os

import tensorflow as tf
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2, preprocess_input, decode_predictions
from tensorflow.keras.preprocessing.image import img_to_array, load_img
import numpy as np


if __name__ == "__main__":
    dossier_json = os.environ.get("DOSSIER_JSON", "/app/data/json")
    fichier_json = f"{dossier_json}/metadata.json"

    image_clustering = ImageClustering(fichier_json)

    # Determine optimal K for RGB clustering
    image_clustering.determine_optimal_k("rgb")

    # Cluster images based on RGB values
    image_clustering.cluster_images("cluster_rgb", "rgb", 5)

    # Cluster images based on RGB variances
    image_clustering.cluster_images("cluster_variance", "variances_rgb", 5)

    # Cluster images based on time of day
    image_clustering.cluster_period()

    # Write metadata to file
    image_clustering.write_metadata(fichier_json)

    print("Clustering Done")
    
    # Tagging
    dossier_images = os.environ.get("DOSSIER_IMAGES", "/app/data/images")

    image_tagger = ImageTagger(dossier_images)
    image_tagger.tag_automatisation()

    print("Tag automatisation Done")

    users_manager = UserManager(dossier_json)
    users_preference = users_manager.create_users(10)
    users_manager.write_preferences(users_preference)



