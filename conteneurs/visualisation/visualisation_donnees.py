import os
from Services.ImageMetadata import ImageMetadata
from Services.ImageFavourites import ImageFavourites

if __name__ == "__main__":

    dossier_json = os.environ.get("DOSSIER_JSON", "/app/data/json")
    fichier_json = f"{dossier_json}/metadata.json"
    user_preference_json = f"{dossier_json}/user_preference.json"

    dossier_charts = os.environ.get("DOSSIER_CHARTS", "/app/data/charts")

    dossier_charts_all = f"{dossier_charts}/all"
    dossier_charts_users = f"{dossier_charts}/users"


    # Visualisation des metadata globales
    image_metadata = ImageMetadata(fichier_json, output_folder=dossier_charts_all)
    image_metadata.visualize()

    # Visualisation des preference utilisateurs
    list(map(
        lambda user_id:  
            ImageFavourites
                (
                user_id=user_id, user_file=user_preference_json, metadata_file=fichier_json, 
                output_folder=f"{dossier_charts_users}/user{user_id}"
                ).visualize()
        , range(10)))
