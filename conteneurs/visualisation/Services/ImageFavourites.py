import json
import matplotlib.pyplot as plt
import pandas as pd
from Services.ImageMetadata import ImageMetadata

class ImageFavourites(ImageMetadata):
    def __init__(self, user_id, user_file, metadata_file, output_folder):
        super().__init__(metadata_file, output_folder)

        self.user_id = user_id
        self.user_preference = self.load_user_preference(user_file)
        self.metadata = self.get_metadata(metadata_file)

        #Créer un DataFrame à partir des données JSON
        self.df = pd.DataFrame.from_dict(self.metadata, orient='index')


    def load_user_preference(self, user_file):
        # Charger les informations des utilisateurs depuis le fichier JSON
        with open(user_file, 'r') as f:
            utilisateurs = json.load(f)
        return utilisateurs

    def get_metadata(self, metadata_file):
        metadata = self.load_metadata(metadata_file)

        utilisateur = self.user_preference[self.user_id]

        # Visualiser les couleurs préférées
        images_preferes = utilisateur['favorite_images']
        print(utilisateur)

        # Extraire les métadonnées des images préférée
        data = dict(filter(lambda item: item[0] in images_preferes, metadata.items()))

        return data


