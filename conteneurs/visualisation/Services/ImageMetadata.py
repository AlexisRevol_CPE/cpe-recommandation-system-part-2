import os
import json
import pandas as pd
import matplotlib.pyplot as plt

class ImageMetadata:
    def __init__(self, metadata_file, output_folder):
        self.metadata = self.load_metadata(metadata_file)
        self.output_folder = output_folder
         #Créer un DataFrame à partir des données JSON
        self.df = pd.DataFrame.from_dict(self.metadata, orient='index')

    
    def visualize_by_year(self):
        try:
            #Créer un DataFrame à partir des données JSON
            df = pd.DataFrame.from_dict(self.metadata, orient='index')

            # Compter l nombre d'images disponibles pour chaque année
            df['DateTime'] = pd.to_datetime(df['DateTime'], errors='coerce')
            #Extraire l'année à partir de la colonne "DateTime"
            df['year'] = pd.to_datetime(df['DateTime']).dt.year.astype(str)
            #Utiliser la méthode "value_counts" pour compter le nombre d'images pour chaque année
            count_by_year = df['year'].value_counts()

            #Créer une figure et un axe pour l'histogramme montrant la fréquence d'apparition des années
            fig, ax = plt.subplots()
            ax.hist(df['year'], bins=len(count_by_year))
            ax.set_title("Fréquence d'apparition des années")
            ax.set_xlabel('Années')
            ax.set_ylabel('Nombre d\'images')
            plt.xticks(rotation=45)
            fig.set_size_inches(10, 8)
            plt.savefig(f"{self.output_folder}/date.png")
            plt.show()
        except:
            pass

    def visualize_by_rgb(self):
        try:
            #Créer un DataFrame à partir des données JSON
            df = pd.DataFrame.from_dict(self.metadata, orient='index')

            #Créer une figure et un axe pour l'histogramme montrant la fréquence d'apparition des couleurs RGB
            fig, ax = plt.subplots()
            # Extraire les valeurs des canaux de couleur RGB et les ajouter à des colonnes séparées dans le DataFrame
            df[["r", "g", "b"]] = df["rgb"].apply(pd.Series)

            # Compter la fréquence d'apparition de chaque couleur RGB
            value_counts = df.groupby(["r", "g", "b"]).size().reset_index(name="count")

            # Créer les colonnes R, G, et B en utilisant apply() et pd.Series()
            df[['R', 'G', 'B']] = df['rgb'].apply(pd.Series)
            # Calculer la fréquence de chaque intensité de chaque composante de couleur
            freq_r = df['R'].value_counts(normalize=True).sort_index()
            freq_g = df['G'].value_counts(normalize=True).sort_index()
            freq_b = df['B'].value_counts(
                normalize=True).sort_index()
            # Tracer une courbe pour chaque fréquence de couleur
            ax.plot(freq_r.index, freq_r.values, color='red', label='R')
            ax.plot(freq_g.index, freq_g.values, color='green', label='G')
            ax.plot(freq_b.index, freq_b.values, color='blue', label='B')
            # Ajouter une légende et des titres
            ax.legend()

            ax.set_xlabel('Intensité')
            ax.set_ylabel('Fréquence')
            ax.set_title('Fréquence des couleurs RGB')
            plt.savefig(f"{self.output_folder}/rgb.png")
            plt.show()
        except:
            pass

    def visualize_by_attribute(self, attribute):
        """
        Plots a bar chart showing the frequency of occurrence of each value in the specified attribute.
        :param attribute: The attribute to visualize.
        """
        # Ignore certain columns that should not be plotted
        ignore_cols = ["DateTime", "filename", "ColorSpace", "rgb", "variances_rgb", "average_rgb", "tags"]

        if attribute in ignore_cols:
            return

        if attribute not in self.df.columns:
            return

        try:
            # Use the value_counts() method to find the frequency of occurrence of each value in the column
            value_counts = self.df[attribute].value_counts()

            # Plot a bar chart for the frequency of occurrence of each value in the column
            fig, ax = plt.subplots()
            ax.bar(value_counts.index, value_counts.values)
            ax.set_title(f"Frequency of occurrence of {attribute}")
            ax.set_xlabel(attribute)
            ax.set_ylabel('Number of images')
            plt.xticks(rotation=45)
            fig.set_size_inches(10, 8)
            plt.show()
            plt.savefig(f"{self.output_folder}/{attribute}.png")
        except Exception as e:
            print(f"Error: {e}")

    def visualize(self):
        """
        Visualiser tout le contenu du fichier metadata.json
        """
        try:
            self.visualize_by_year()
            self.visualize_by_rgb()

            # Compter le nombre pour les différents types : taille de l'image, l'orientation des images, les modèles d'appareils photo, etc.
            list(map(self.visualize_by_attribute, self.df.columns))
            # Compter le nombre pour les différents types : taille de l'image, l'orientation des images, les modèles d'appareils photo, etc.
            # for col in self.df.columns:
            #    self.visualize_by_attribute(col)
        except:
            pass


    def load_metadata(self, metadata_file):
        with open(metadata_file, "r") as f:
            return json.load(f)