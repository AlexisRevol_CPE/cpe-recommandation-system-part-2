import os
import requests
import shutil
import time
from tqdm import tqdm
from pyspark.sql.functions import udf
from pyspark.sql.types import IntegerType

class ImageDownloader:
    def __init__(self):
        print("Image Downloader create")
        #self.counter = 0
        self.headers = {"User-Agent": "Mozilla/5.0"}
        self.images_dir = os.environ.get("DOSSIER_IMAGES", "/app/data/images")

    def save(self, response, filename):
        path = os.path.join(self.images_dir, filename)
        with open(path, "wb") as image:
            response.raw.decode_content = True
            shutil.copyfileobj(response.raw, image)
            print(f"{filename} downloaded")

    def download_image(self, counter, url, max_retries=3, sleep_time=1):
        num_retries = 0
        while num_retries < max_retries:
            try:
                response = requests.get(url, allow_redirects=True, headers=self.headers, stream=True)
                if response.status_code == 200:
                    filename = f"img{counter}.jpg"
                    self.save(response, filename)
                    return response.content
                else:
                    print(f"Request failed with status code: {response.status_code}")
            except Exception as e:
                print(f"Request failed with exception: {e}")
            num_retries += 1
            time.sleep(sleep_time)
        print("Max retries exceeded. Aborting...")

    def old_download_image(self, counter, url):
        """
        Download an image from an URL and save it to the 'images/' directory
        """
        request = requests.get(url, allow_redirects=True, headers=self.headers, stream=True)
        if request.status_code == 200:
            filename = f"img{counter}.jpg"
            path = os.path.join(self.images_dir, filename)
            with open(path, "wb") as image:
                request.raw.decode_content = True
                shutil.copyfileobj(request.raw, image)
                print(f"{filename} downloaded")
        else:
            print(f"Error downloading {url}. Status code: {request.status_code}")
        return request.status_code

    def download_images(self, dataframe):
        try:
            # Define a PySpark UDF to download the images
            dataframe.select("index", "image").foreach(self.download) 
            print("\nDone")
        except Exception as e:
            print(f"Error downloading images : {e}")

    def download(self, row):
        self.download_image(row['index'], row['image'])