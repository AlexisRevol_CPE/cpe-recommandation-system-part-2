import sys
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark.sql.types import StringType
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from pyspark.sql.functions import monotonically_increasing_id


class WikidataImageDownloader:
    def __init__(self, endpoint_url):
        self.endpoint_url = endpoint_url
        self.spark = SparkSession.builder.appName("WikidataImageDownloader").getOrCreate()

    def get_results(self, query):
        """
        Get results from a SPARQL query
        """
        user_agent = "WDQS-example Python/%s.%s" % (
            sys.version_info[0],
            sys.version_info[1],
        )
        sparql = SPARQLWrapper(self.endpoint_url, agent=user_agent)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        return sparql.query().convert()

    def get_image_data(self):
        # Get cities
        query = """SELECT DISTINCT ?grandeville ?grandevilleLabel ?pays ?paysLabel ?image {
          ?grandeville wdt:P31 wd:Q1549591;
                       wdt:P17 ?pays;
                       wdt:P18 ?image.
         SERVICE wikibase:label { bd:serviceParam wikibase:language "fr". }
        }
        LIMIT 100"""

        results = self.get_results(query)

        # Create Spark DataFrame from results
        array = [(result["grandevilleLabel"]["value"],
                  result["paysLabel"]["value"],
                  result["image"]["value"]) for result in results["results"]["bindings"]]
        rdd = self.spark.sparkContext.parallelize(array)
        df = rdd.toDF(["ville", "pays", "image"])

        # Convert string columns to the appropriate types
        df = df.withColumn("ville", col("ville").cast("string"))
        df = df.withColumn("pays", col("pays").cast("string"))
        df = df.withColumn("image", col("image").cast("string"))
        df = df.withColumn("index", monotonically_increasing_id())

        return df
