from Services.WikidataImageDownloader import WikidataImageDownloader
from Services.ImageDownloader import ImageDownloader

if __name__ == '__main__':
    endpoint_url = "https://query.wikidata.org/sparql"

    wikidata_downloader = WikidataImageDownloader(endpoint_url)
    image_data = wikidata_downloader.get_image_data()
    
    image_downloader = ImageDownloader()
    image_downloader.download_images(image_data)
    
