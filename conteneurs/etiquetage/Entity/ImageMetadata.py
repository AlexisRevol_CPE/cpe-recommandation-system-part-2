from PIL import Image, ExifTags, ImageOps
from PIL.ExifTags import TAGS
import json
import os
import colorsys
from Entity.ImageColor import ImageColor
from datetime import datetime

class ImageMetadata:
    def __init__(self, image_path):
        print(f"Loading metadata of {image_path}")
        self.image_path = image_path
        self.img = Image.open(image_path)
        self.filename = os.path.basename(image_path)
        self.size = self._get_size_data()
        self.format = self.img.format
        self.mode = self.img.mode
        self.metadata = {
            'filename': self.filename,
            'size': self.size,
            'format': self.format,
            'type': self.mode,
            'mode': 'unknown',
            'Software': 'unknown',
            'DateTime': str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
            'ColorSpace': 'unknown',
            'Model': 'unknown',
            'rgb': 'unknown',
            'variances_rgb': 'unknown',
            'average_rgb': 'unknown'
        }
        self.exif_data = self._get_exif_data()
        self.color_data = self._get_color_data()

    def _get_size_data(self):
        """
        Return size data according to its digital value
        """
        width, height = self.img.size
        if width <= 150 and height <= 150:
            return 'thumbnail'
        elif width <= 800 and height <= 800:
            return 'medium'
        else:
            return 'large'

    def _get_orientation_data(self, orientation):
        """
        Return orientation tag according to exif value
        """
        if orientation == 3 or orientation == 6 or orientation == 8:
            return 'landscape'
        else:
            return 'portrait'

    def _get_date_data(self, value):
        """
        Return date in format '%Y-%m-%d %H:%M:%S'
        """
        datetime_object = datetime.strptime(value, '%Y:%m:%d %H:%M:%S')
        return datetime_object.strftime('%Y-%m-%d %H:%M:%S')


    def _get_exif_data(self):
        """
        Return image exif data
        """
        try:
            exif_data = self.img._getexif()
            if exif_data:
                # Get valid exif
                dict_data = dict(filter(lambda tag_value: tag_value[0] in TAGS, exif_data.items())).items()
                # Apply exif to metadata
                exif_data = list(map(lambda tag_value: self.set_metadata(*tag_value), dict_data))
            return exif_data
        except Exception as e:
            return None
        
    def set_metadata(self, exif_value, value):
        tag_name = TAGS[exif_value]
        if tag_name == "ColorSpace" or tag_name == "Software" or tag_name == "Model":
            self.metadata[tag_name] = value
        elif tag_name == "DateTime":
            self.metadata[tag_name] = self._get_date_data(value)
        elif tag_name == "Orientation":
            self.metadata['mode'] = self._get_orientation_data(value)


    def _get_color_data(self):
        """
        Get color information of img
        """
        image_color = ImageColor(self.img)
        color_data = image_color.get_colors_data()
        self.metadata.update(color_data)

        return color_data

    def get_metadata(self):
        return self.metadata