from PIL import Image, ExifTags, ImageOps
from PIL.ExifTags import TAGS
import json
import os
import colorsys
import numpy as np

class ImageColor:
    """
    Compute color analys of given image
    """
    def __init__(self, image):
        try:
            self.img = ImageOps.fit(image, (400, 400), Image.ANTIALIAS)
            self.rgb = self.get_rgb_color()
            self.average_rgb = self.get_average_rgb()
            self.variances_rgb = self.get_variance_rgb()
        except:
            self.img = None
            self.rgb = [-1, -1, -1]
            self.average_rgb = [-1, -1, -1]
            self.variances_rgb = [-1, -1, -1]

    
    def get_rgb_color(self):
        """
        Return the dominant color of an image using histogram
        """
        # Calculer l'histogramme
        hist = self.img.histogram()

        # Normaliser l'histogramme
        total_pixels = self.img.size[0] * self.img.size[1]

        hist_norm = list(map(lambda h: float(h) / total_pixels, hist))


        # Trouver la couleur dominante
        max_value = max(hist_norm)
        max_index = hist_norm.index(max_value)
        color_hsv = (max_index / 255.0, 1.0, 1.0)

        # Convertir la couleur en RGB
        color_rgb = tuple(map(lambda c: int(c * 255), colorsys.hsv_to_rgb(*color_hsv)))

        return color_rgb

    def get_average_rgb(self):
        """
        Return the average of red, blue and green and their variances of the selected image
        """
        # Récupérer les données des pixels
        pixels = np.array(self.img.getdata())

        # Calculer les moyennes
        avg_red, avg_green, avg_blue, *_ = np.mean(pixels, axis=0)

        liste_avg = [avg_red, avg_green, avg_blue]
        return liste_avg

    
    def get_variance_rgb(self):
        """
        Return the average of red, blue and green and their variances of the selected image
        """
        # Récupérer les données des pixels
        pixels = np.array(self.img.getdata())

        # Calculer les variances
        var_red, var_green, var_blue,*_ = np.var(pixels, axis=0)
        
        liste_variance = [var_red, var_green, var_blue]
        return liste_variance


    def get_colors_data(self):
        metadata = {
            "rgb": list(map(int, self.rgb)),
            "average_rgb": list(map(int, self.average_rgb)),
            "variances_rgb": list(map(int, self.variances_rgb))
        }
        return metadata
