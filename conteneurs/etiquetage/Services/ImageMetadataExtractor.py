import json
import os
import colorsys
from Entity.ImageMetadata import ImageMetadata
from tqdm import tqdm
from functools import reduce

class ImageMetadataExtractor:
    def __init__(self, folder_path):
        self.folder_path = folder_path
        self.metadata_all = {}

    def extract_metadata(self):
        """
        Extract metadata of all image in the folder
        """
        # Créer une liste de noms de fichiers correspondants aux extensions de fichier spécifiées
        filenames = list(filter(lambda filename: filename.endswith(('.jpg', '.jpeg', '.png')), os.listdir(self.folder_path)))

        # Appliquer la fonction os.path.join() à chaque nom de fichier
        paths = list(map(lambda filename: os.path.join(self.folder_path, filename), filenames))

        # Créer une liste d'objets ImageMetadata
        image_metadata_list = list(map(lambda path: ImageMetadata(path), paths))

        # Obtenir les métadonnées de chaque objet ImageMetadata
        metadata_list = list(map(lambda image_metadata: image_metadata.get_metadata(), image_metadata_list))

        # Associer chaque nom de fichier avec ses métadonnées correspondantes
        self.metadata_all = dict(zip(filenames, metadata_list))

        print()
        return self.metadata_all


    def save_metadata_to_json(self, output_file):
        """
        Save a dictionary containing metadata in an output file
        """
        with open(output_file, 'w') as f:
            json.dump(self.metadata_all, f, indent=4)
