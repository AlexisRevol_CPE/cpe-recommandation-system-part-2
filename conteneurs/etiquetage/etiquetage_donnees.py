from Services.ImageMetadataExtractor import ImageMetadataExtractor
import os
if __name__ == '__main__':
    dossier_images = os.environ.get("DOSSIER_IMAGES", "/app/data/images")
    dossier_json = os.environ.get("DOSSIER_JSON", "/app/data/json")

    image_extractor = ImageMetadataExtractor(dossier_images)
    image_extractor.extract_metadata()
    image_extractor.save_metadata_to_json(output_file=f"{dossier_json}/metadata.json")





    