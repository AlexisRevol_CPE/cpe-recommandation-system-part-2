#!/bin/bash

# Définir les couleurs pour les messages
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # Couleur par défaut

# Afficher un message en jaune
echo -e "${YELLOW}Arrêt des conteneurs Docker...${NC}"

# Arrêter les conteneurs Docker
docker compose down

# Afficher un message en vert
echo -e "${GREEN}Suppression du contenu du dossier 'volumes/data/images'...${NC}"

# Supprimer le contenu du dossier "volumes/data/images" sauf le .gitkeep
#rm -rf volumes/data/images/*
find volumes/data/images/ ! -name '.gitkeep' -type f -delete

#rm -v  volumes/data/images/* !(".gitkeep") 

# Afficher un message en vert
echo -e "${GREEN}Suppression du contenu du dossier 'volumes/data/json'...${NC}"
find volumes/data/json/ ! -name '.gitkeep' -type f -delete

# Afficher un message en vert
echo -e "${GREEN}Suppression du contenu du dossier 'volumes/data/charts/all'...${NC}"
find volumes/data/charts/all ! -name '.gitkeep' -type f -delete

echo -e "${GREEN}Suppression du contenu du dossier 'volumes/data/charts/users'...${NC}"
find volumes/data/charts/users ! -name '.gitkeep' -type f -delete

echo -e "${GREEN}Suppression du contenu du dossier 'volumes/data/charts/classification'...${NC}"
find volumes/data/charts/classification ! -name '.gitkeep' -type f -delete

# Afficher un message en rouge
echo -e "${RED}Opération terminée.${NC}"
