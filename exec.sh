#!/bin/bash

# Define colors
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color

# Permission for data 
#chmod a+=rwx volumes/data

# Print colored messages
echo -e "${GREEN}Building Docker images...${NC}"
docker compose build
if [ $? -eq 0 ]; then
  echo -e "${GREEN}Docker images successfully built!${NC}"
else
  echo -e "${RED}Failed to build Docker images.${NC}"
  exit 1
fi

# Lancer les conteneurs dans l'ordre
echo -e "${YELLOW}Starting Docker containers...${NC}"

# Lancer le conteneur acquisition
docker compose run acquisition

# Attendre que le conteneur acquisition soit en cours d'exécution
while [ "$(docker-compose ps -q acquisition)" != "" ]; do
  sleep 1
done

# Lancer le conteneur etiquetage
docker compose run etiquetage

# Attendre que le conteneur etiquetage soit terminé
while [ "$(docker-compose ps -q etiquetage)" != "" ]; do
  sleep 1
done

# Lancer le conteneur analyse
docker compose run analyse

# Attendre que le conteneur analyse soit terminé
while [ "$(docker-compose ps -q analyse)" != "" ]; do
  sleep 1
done

# Lancer le conteneur recommandation
docker compose run recommandation

# Attendre que le conteneur recommandation soit terminé
while [ "$(docker-compose ps -q recommandation)" != "" ]; do
  sleep 1
done

if [ $? -eq 0 ]; then
  echo -e "${GREEN}Docker containers started!${NC}"
else
  echo -e "${RED}Failed to start Docker containers.${NC}"
  exit 1
fi
